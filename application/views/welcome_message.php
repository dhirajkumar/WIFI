<!DOCTYPE html>
<HTML>
    <HEAD>
    	<TITLE> WIFI </TITLE>
    	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>source/datatables/jquery.dataTables.css">
      	<link rel="stylesheet" href="<?php echo base_url();?>source/bootstrap/css/bootstrap.min.css">
        <script src="<?php echo base_url();?>source/jQuery-2.1.4.min.js"></script>
      	<script type="text/javascript" src="<?php echo base_url();?>source/datatables/jquery.dataTables.js"/></script>
        <script type="text/javascript" charset="utf8" src="<?php echo base_url();?>source/datatables/dataTables.buttons.min.js"></script>
        <script src="<?php echo base_url();?>source/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>source/datatables/dataTables.bootstrap.min.js"></script>
       
        <link href = "<?php echo base_url(); ?>source/jquery-ui.css"
         rel = "stylesheet">
        <script src = "<?php echo base_url(); ?>source/jquery-ui.js"></script>
      
   </script>

    </HEAD>
    <BODY>
    <div >
       <div class="alert alert-info" ><Center><b>Wi-Fi location</b></Center></div>
         <table id="example1" class="table table-bordered table-striped table-hovered nowrap table-hover responsive display"  width="100%">
                <thead>
                <tr class="bg-primary">
                  <th>MAC</th>
                  <th>Time</th>
                  <th>Floor</th>
                  <th>Graph</th>
                 </tr>

                </thead>
                <tbody>
             <tfoot>
               
            </tfoot>
            </table>    
     </div>
      </BODY>
</HTML>

<script type="text/javascript">
  var table = $("#example1").DataTable({
         "bDestroy": true,
         "retrieve": true,
         "responsive": false,
         "dom": 'rftp',
         'bLengthChange': false,
         "sAjaxSource":"http://localhost:8081/WiFi/index.php/Welcome/fetch_data",
         "bProcessing":false,
         "ordering": false,
         "bLengthChange" : false,
              
        "bServerSide":true,
        "bInfo": false,
        "bFilter": false,
        "oLanguage": {
        "sEmptyTable": "No data found "
      },
   "columnDefs": [
    { "width": "20%", "targets": 0 },
    { "width": "20%", "targets": 1 },
    { "width": "20%", "targets": 2 }
  ],
       
     //"bPaginate": false,
     "bJQueryUI": false,
       
      "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
        console.log(aData);
        console.log(iDisplayIndex);

        
       $('td:eq(0)', nRow).html(aData[0]);
       $('td:eq(1)', nRow).html(aData[1]);
       $('td:eq(2)', nRow).html(aData[2]);
       $('td:eq(3)', nRow).html("<div id='progressbar1' class='ui-progressbar ui-widget ui-widget-content ui-corner-all' role='progressbar' aria-valuemin='0' aria-valuemax='100' aria-valuenow='60'><div class='ui-progressbar-value ui-widget-header ui-corner-left' style='width: "+((aData[3].replace('-',''))*100)/100+"%;'>"+aData[3]+"</div></div>");
      
    },
        

    "fnServerData": function ( sSource, aoData, fnCallback ) {
      $.ajax({
        "dataType": 'json',
        "type": "POST",
        "url": sSource,
        "data": aoData,
        "success": fnCallback
      });
    },
    
     language: {search: "_INPUT_",searchPlaceholder: "Search.."},
        
  });


    function autoRefresh()
   {
     table.ajax.reload( null, false );
   }

// setInterval('autoRefresh()', 1000);
</script>
