<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class wi_fi_model extends CI_Model
{
	
	public function __construct()
	{
		parent:: __construct();
	}
	public function fetch_data()
	{
		$this->db->select('wifi_rissi_mac.MAC,wifi_rissi_mac.Time,wifi_rissi_mac.DataFrom,wifi_rissi_mac.RSSI');
		$this->db->from('wifi_rissi_mac');
		$this->db->group_by('wifi_rissi_mac.MAC');
	    $query = $this->db->get();
	    return $query->result();
	}
}
?>